#pragma once
#include <vector>
#include <cstdlib>
#include <ctime>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

static const unsigned int max_size = 10;
static const double wall_density = 0.2;

struct JSONData {
    int h = -1;
    int w = -1;
    int sr = -1;
    int sc = -1;
    int er = -1;
    int ec = -1;
    std::vector<std::pair<int, int>> walls;
};

void load(char* file, JSONData& dataOut) {

    namespace pt = boost::property_tree;

    pt::ptree loadJson;
    pt::ptree start;
    pt::ptree end;
    pt::ptree walls_tree;

    pt::read_json(file, loadJson);

        dataOut.h = loadJson.get_child("height").get_value<int>();
        dataOut.w = loadJson.get_child("width").get_value<int>();
        start = loadJson.get_child("start");
        end = loadJson.get_child("end");
        walls_tree = loadJson.get_child("walls");

        dataOut.sr = start.get_child("row").get_value<int>();
        dataOut.sc = start.get_child("column").get_value<int>();
        dataOut.er = end.get_child("row").get_value<int>();
        dataOut.ec = end.get_child("column").get_value<int>();

        for(auto& w : walls_tree) {
            int r = w.second.get_child("row").get_value<int>();
            int c = w.second.get_child("column").get_value<int>();
            dataOut.walls.push_back({r, c});
        }
}

void generate(JSONData& dataOut) {

    int & h = dataOut.h;
    int & w = dataOut.w;
    int & sr = dataOut.sr;
    int & sc = dataOut.sc;
    int & er = dataOut.er;
    int & ec = dataOut.ec;

    srand(time(nullptr));
    h = rand() % (max_size - 1) + 2;
    w = rand() % (max_size - 1) + 2;
    sr = rand() % h;
    sc = rand() % w;
    er = rand() % h;
    ec = rand() % w;
    while (er == sr && ec == sc) {
        er = rand() % h;
        ec = rand() % w;
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            double value = (rand() % 101) / 100.0;
            //std::cout << value << std::endl;
            if (value < wall_density && !(i == sr && j == sc) && !(i == er && j == ec))
                dataOut.walls.push_back({i, j});
        }
    }
}