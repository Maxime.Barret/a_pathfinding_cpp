#include <iostream>
#include <string>
#include <vector>


#include "APathFinding.hpp"
#include "utils.hpp"


int main(int argc, char* argv[]) {

    JSONData data;

    // Get info from json file
    if (argc == 2) {
        load(argv[1], data);
    }
    // Generate a random scenario
    else if (argc == 1) {
        generate(data);
    }
    
    
    APathFinding *grd = new APathFinding(data.h, data.w);
    grd->setStart(data.sr, data.sc);
    grd->setEnd(data.er, data.ec);
    grd->setWalls(data.walls);

    std::cout << "Environnement : " << std::endl;
    std::cout << *grd << std::endl;

    std::vector<Cell*> path;
    grd->compute(path);


    if (!path.empty()) {
        std::cout << "Path found : " << std::endl;
        for (Cell* c : path)
            std::cout << "\t(" << c->row << "," << c->col << ")" << std::endl;
    }
    

    delete grd;
    return EXIT_SUCCESS;
}