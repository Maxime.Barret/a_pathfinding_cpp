#pragma once
#include "includes.hpp"

void test_display_wall()
{
    APathFinding *grd = new APathFinding(5, 5);
    grd->setWalls({{0, 0}, {2, 1}}); 
    std::stringstream ss { };
    ss << *grd;
    
    /* #....
       .....
       .#...
       .....
       .....

     */
    std::string truth { "" };
    for (unsigned int i = 0; i < 5; i++) {
        for (unsigned int j = 0; j < 5; j++) {
            if ((i == 0 && j == 0) || (i == 2 && j == 1)) 
                truth += APathFinding::symbol(APathFinding::CodeWall);
            else
                truth += APathFinding::symbol(APathFinding::CodeEmpty);
        }
        truth += "\n";
    }

    if (ss.str() != truth) {
        std::cout << "DISPLAY_WALL => KO" << std::endl;
        std::cout << "grid : " << std::endl << *grd << std::endl;
    }
    else 
        std::cout << "DISPLAY_WALL => OK" << std::endl;

    delete grd;
}
