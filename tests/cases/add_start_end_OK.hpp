#pragma once
#include "includes.hpp"

void test_add_start_and_end_OK() 
{
    const unsigned int h = 3;
    const unsigned int w = 4;

    APathFinding *grd = new APathFinding(h, w);

    try {
        grd->setStart(0, 0);
        grd->setEnd(2, 3);
        std::cout << "START & END TEST OK => OK" << std::endl;
    }
    catch (std::runtime_error& e) {
        std::cerr << "START & END TEST OK => KO : " << e.what() << std::endl;
    }
    delete grd;
}