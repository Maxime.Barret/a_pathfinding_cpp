#pragma once
#include "includes.hpp"

void test_given_grid() 
{
    const unsigned int h = 3;
    const unsigned int w = 4;

    APathFinding *grd = new APathFinding(h, w);
    std::stringstream ss { };
    ss << *grd;

    /* ....
       ....
       .... 

    */
    std::string truth { "" };
    for (unsigned int i = 0; i < h; i++) {
        for (unsigned int j = 0; j < w; j++) {
            truth += ".";
        }
        truth += "\n";
    }

    if (ss.str() != truth) {
        std::cout << "EMPTY_GIVEN_TEST => KO" << std::endl;
        std::cout << "grid : " << std::endl << *grd << std::endl;
    }
    else 
        std::cout << "EMPTY_GIVEN_TEST => OK" << std::endl;
    
    delete grd;
}