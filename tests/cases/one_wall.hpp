#pragma once
#include "includes.hpp"

void test_one_wall()
{
    APathFinding *grd = new APathFinding(5, 5);

    try {
        grd->setWall(0, 0);
        std::cout << "ONE_WALL_TEST => OK" << std::endl;
    }
    catch (std::runtime_error& e) {
        std::cerr << "ONE_WALL_TEST => KO : " << e.what() << std::endl;
    }
    delete grd;
}