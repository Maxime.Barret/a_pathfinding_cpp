#pragma once
#include "includes.hpp"

void test_add_start_and_end_KO() 
{
    const unsigned int h = 3;
    const unsigned int w = 4;

    APathFinding *grd = new APathFinding(h, w);

    try {
        grd->setStart(0, 4);
        grd->setEnd(2, 3);
        std::cerr << "START & END TEST KO => KO : No exception raised" << std::endl;
    }
    catch (std::runtime_error& e) {
        std::cout << "START & END TEST KO => OK" << std::endl;
    }
    delete grd;
}