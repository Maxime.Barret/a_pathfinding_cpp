#pragma once
#include "includes.hpp"

void test_empty_grid() 
{
    APathFinding *grd = new APathFinding();
    std::stringstream ss { };

    ss << *grd;
    if (ss.str() != "") {
        std::cout << "EMPTY_GRID_TEST => KO" << std::endl;
        std::cout << "grid : " << std::endl << *grd << std::endl;
    }
    else 
        std::cout << "EMPTY_GRID_TEST => OK" << std::endl;
    
    delete grd;
}