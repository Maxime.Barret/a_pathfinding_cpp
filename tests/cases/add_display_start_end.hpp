#pragma once
#include "includes.hpp"

void test_display_with_start_end() 
{
    const unsigned int h = 3;
    const unsigned int w = 4;
    const unsigned int spr = 0, spc = 0, epr = 2, epc = 3;

    APathFinding *grd = new APathFinding(h, w);
    grd->setStart(spr, spc);
    grd->setEnd(epr, epc);
    std::stringstream ss { };
    ss << *grd;
    
    /* S...
       ....
       ...E 

     */
    std::string truth { "" };
    for (unsigned int i = 0; i < h; i++) {
        for (unsigned int j = 0; j < w; j++) {
            if (i == spr && j == spc) 
                truth += APathFinding::symbol(APathFinding::CodeStart);
            else if (i == epr && j == epc)
                truth += APathFinding::symbol(APathFinding::CodeEnd);
            else
                truth += ".";
        }
        truth += "\n";
    }

    if (ss.str() != truth) {
        std::cout << "DISPLAY_START_END => KO" << std::endl;
        std::cout << "grid : " << std::endl << *grd << std::endl;
    }
    else 
        std::cout << "DISPLAY_START_END => OK" << std::endl;

    delete grd;
}