#pragma once
#include "includes.hpp"

void test_overwrite()
{
    APathFinding *grd = new APathFinding(5,5);
    grd->setStart(0, 0);
    grd->setEnd(4, 4);
    
    try {
        grd->setWall(0, 0);
        std::cerr << "OVERWRITE_TEST => KO : No exception raised" << std::endl;
    }
    catch (std::exception& e) {
        std::cout << "OVERWRITE_TEST => OK" << std::endl;
    }

    delete grd;
}