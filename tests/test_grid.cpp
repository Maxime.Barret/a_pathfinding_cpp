#include "cases/test_cases.hpp"

int main() {

    test_empty_grid();
    test_given_grid();
    test_add_start_and_end_OK();
    test_add_start_and_end_KO();
    test_display_with_start_end();
    test_one_wall();
    test_display_wall();
    test_overwrite();
    return EXIT_SUCCESS;
}