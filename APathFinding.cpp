#include "APathFinding.hpp"
#include <algorithm>
#include <sstream>
#include <queue>
#include <cmath>




double distance(const Cell* c1, const Cell* c2) {
    return sqrt(pow((c2->row - c1->row), 2.0)
                + pow((c2->col - c1->col), 2.0));
}


APathFinding::APathFinding() : height_(0), width_(0) 
{
    grid = nullptr;
}

APathFinding::APathFinding(const int height, const int width) : height_(height), width_(width) 
{
    // Construction en bloc memoire adjacent
    grid = new Cell**[height_];
    if (height_)
    {
        grid[0] = new Cell*[height_ * width_];
        for (int i = 1; i < height_; ++i) {
            grid[i] = grid[0] + i * width_;
        }
            

        for (int i = 0; i < height_; ++i) {
            for (int j = 0; j < width_; ++j)
                grid[i][j] = new Cell(i, j, APathFinding::CodeEmpty);
        }
    }
}

APathFinding::~APathFinding() 
{
    startingNode = nullptr;
    endingNode = nullptr;
    for (int i = 0; i < height_; ++i) {
        for (int j = 0; j < width_; ++j)
            delete grid[i][j];
    }
    if (height_) delete [] grid[0];
    delete [] grid;

}

std::ostream& operator<<(std::ostream& out, const APathFinding& grid) 
{
    for (int i = 0; i < grid.height_; i++) {
        for (int j = 0; j < grid.width_; j++) {
            out << APathFinding::symbol(grid.grid[i][j]->code) << "  ";
        }
        out << std::endl;
    }
    return out;
}

/*
    Return a symbol to display according to the type of a cell

*/
char APathFinding::symbol(const int code) 
{
    char sym;
    switch(code) {
        case APathFinding::CodeEmpty : 
            sym = '.'; // empty cell
            break;
        case APathFinding::CodeWall :
            sym = '#'; // wall
            break;
        case APathFinding::CodeStart : 
            sym = 'S'; // Starting point
            break;
        case APathFinding::CodeEnd :
            sym = 'E'; // Ending point
            break;
        case APathFinding::CodePath :
            sym = 'o';
            break;
        default : 
            sym = 'x';
            break;
    }   
    return sym;
}

/*
    Pretty print a cell coordonate

*/
std::string APathFinding::printRC(const int row, const int col) const
{
    std::stringstream ss;
    ss << "\tRow : " << row << "/" << height_ << std::endl;
    ss << "\tCollumn : " << col << "/" << width_ << std::endl;
    return ss.str();
}

/*
    Verify that a given cell is not out of the grid

*/
bool APathFinding::canAccess(const int row, const int col) const 
{
    if (row >= height_ || row < 0)
        return false;
    if (col >= width_ || col < 0)
        return false;
    return true;
}

void APathFinding::setStart(const int row, const int col)
{
    if (!canAccess(row, col))
        throw std::runtime_error("Starting point out of the grid :\n" + printRC(row, col));

    if (grid[row][col]->code != APathFinding::CodeEmpty)
        throw std::runtime_error("Cell " + std::to_string(row) + "/" + std::to_string(col) + " not empty : Cannot set start point");

    grid[row][col]->code = APathFinding::CodeStart;
    grid[row][col]->gCost = 0;
    grid[row][col]->hCost = 0;

    startingNode = grid[row][col];
}

void APathFinding::setEnd(const int row, const int col)
{
    if (!canAccess(row, col))
        throw std::runtime_error("Ending point out of the grid :\n" + printRC(row, col));
        
    if (grid[row][col]->code != APathFinding::CodeEmpty)
        throw std::runtime_error("Cell " + std::to_string(row) + "/" + std::to_string(col) + " not empty : Cannot set end point");
        
    grid[row][col]->code = APathFinding::CodeEnd;
    endingNode = grid[row][col];
}

void APathFinding::setWall(const int row, const int col)
{
    if (!canAccess(row, col))
        throw std::runtime_error("Wall out of the grid :\n" + printRC(row, col));
        
    if (grid[row][col]->code != APathFinding::CodeEmpty)
        throw std::runtime_error("Cell " + std::to_string(row) + "/" + std::to_string(col) + " not empty : Cannot add wall");

    grid[row][col]->code = APathFinding::CodeWall;
}

void APathFinding::setWalls(const std::vector<std::pair<int, int>> l)
{
    for(const auto p : l)
    {
        try {
            setWall(std::get<0>(p), std::get<1>(p));
        }
        catch (std::runtime_error& e) {
            throw e;
        } 
    }       
}

void APathFinding::clearCell(const int row, const int col) 
{
    if(canAccess(row, col)) {
        grid[row][col]->code = APathFinding::CodeEmpty;
    }
}

void APathFinding::clear() {
    for(int i = 0; i < height_; i++)
        for(int j = 0; j < width_; j++)
            clearCell(i, j);
}


void APathFinding::computePath(std::vector<Cell*>& path) {
    Cell* node = endingNode->parent;
    while (node != startingNode) {
        path.push_back(node);
        node->code = APathFinding::CodePath;
        node = node->parent;
    }

}



void APathFinding::compute(std::vector<Cell*>& path) 
{
    std::priority_queue<Cell*, std::vector<Cell*>, fCostSort> openList;
    openList.emplace(startingNode);

    while (!openList.empty()) {
        Cell* q = openList.top();
        openList.pop();
        //std::cout << "Parent " << q->row << "," << q->col << std::endl;

        for (int add_r = -1; add_r <=1; add_r++) {
            for (int add_c = -1; add_c <= 1; add_c++) {
                if (canAccess(q->row + add_r, q->col + add_c) && !(add_r == 0 && add_c == 0)) {
                    Cell* neighbour = grid[q->row + add_r][q->col + add_c];
                    //std::cout << "\tProcessing " << neighbour->row << "," << neighbour->col << std::endl;
                    if (neighbour->code == APathFinding::CodeEnd) {
                        neighbour->parent = q;
                        computePath(path);
                        std::cout << "Ending point reached" << std::endl;
                        std::cout << *this << std::endl;
                        return;
                    }
                    else if (neighbour->code != APathFinding::CodePath && neighbour->code != APathFinding::CodeWall) {
                        double newGcost, newHcost;
                        newGcost = q->gCost + (isDiagonal(add_r, add_c) ? sqrt(2) : 1);
                        newHcost = distance(neighbour, endingNode);

                        if (neighbour->gCost + neighbour->hCost == -2 || neighbour->gCost + neighbour->hCost > newGcost + newHcost) {
                            neighbour->gCost = newGcost;
                            neighbour->hCost = newHcost;
                            neighbour->parent = q;
                            openList.emplace(neighbour);
                        }
                    }
                }
            }
        }
    }
    std::cout << "Failed to find a path" << std::endl;
}

bool isDiagonal(const int add_r, const int add_c) {
    return ((add_r + add_c) * (add_r + add_c) == 4) || ((add_r + add_c) * (add_r + add_c) == 0);
}