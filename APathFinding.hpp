#pragma once
#include <iostream>
#include <string>
#include <vector>

//static int nbCell = 0;

bool isDiagonal(const int add_r, const int add_c);


struct Cell {
    Cell(const int r, const int co, const int c) : code(c), row(r), col(co) {/*++nbCell; std::cout << nbCell << " Cell" << std::endl;*/}
    ~Cell() { /*--nbCell; std::cout << nbCell << " Cell" << std::endl;*/}
    int gCost = -1;
    double hCost = -1;
    Cell *parent = nullptr;
    int code = 0;
    int row = -1;
    int col = -1;
};

struct fCostSort {
    bool operator()(const Cell* c1, const Cell* c2) {
        if (c1->hCost + c1->gCost > c2->hCost + c2->gCost)
            return true;
        else if (c1->hCost + c1->gCost == c2->hCost + c2->gCost)
            return c1->hCost > c2->hCost;
        else
            return false;
    }
};

double distance(const Cell* c1, const Cell* c2);


// Class that prints a grid on the console
class APathFinding {
public:
    static const int CodeEmpty = 0;
    static const int CodeWall  = 1;
    static const int CodeStart = 2;
    static const int CodeEnd   = 3;
    static const int CodePath  = 4;
public:
    APathFinding();
    APathFinding(const int height, const int width);

    ~APathFinding();

    void setStart(const int row, const int col);
    void setEnd(const int row, const int col);
    void setWall(const int row, const int col);
    void setWalls(const std::vector<std::pair<int, int>> l);
    void clearCell(const int row, const int col);
    void clear();

    void compute(std::vector<Cell*>& path);
    
public:
    // Give the symbol to display according to the type of the cell
    static char symbol(const int cell);

    friend std::ostream& operator<<(std::ostream& out, const APathFinding& grid);
private:
    int height_;
    int width_;
    Cell ***grid;
    Cell* startingNode;
    Cell* endingNode;

private:
    bool canAccess(const int row, const int col) const;
    std::string printRC(const int row, const int col) const;
    void computePath(std::vector<Cell*>& path);
};